# Curso Python para no programadores

Curso introductorio de Python.

[Curso](https://www.educacionit.com/curso-de-introduccion-python)  
> [Prácticas y ejercicios Clase 01](https://gitlab.com/diegobollini/curso-python-para-no-progamadores/-/tree/master/Clase%2001)  
> [Prácticas y ejercicios Clase 02](https://gitlab.com/diegobollini/curso-python-para-no-progamadores/-/tree/master/Clase%2002)  
> [Prácticas y ejercicios Clase 03](https://gitlab.com/diegobollini/curso-python-para-no-progamadores/-/tree/master/Clase%2003)  
> [Prácticas y ejercicios Clase 04](https://gitlab.com/diegobollini/curso-python-para-no-progamadores/-/tree/master/Clase%2004)  
> [Prácticas y ejercicios Clase 05](https://gitlab.com/diegobollini/curso-python-para-no-progamadores/-/tree/master/Clase%2005)  

[Programa](https://www.educacionit.com/generar-pdf-curso?toc=introduccion-python)  
[Lab y ejercicios extra](https://drive.google.com/drive/folders/1hjdyRkGjymvAPOP82DXF4RRRGKBG0f0-)  
[Apuntes muy bonitos hechos por @majoledes](https://losapuntesdemajo.now.sh/)
