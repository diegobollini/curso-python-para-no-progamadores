# I6- Calculadora: Ingresar dos números y un operador(+,-,*,/) por consola
#  e imprimir el resultado de la siguiente manera: Resultado: 3 + 4 = 7

print("Gracias por usar la calculadora!")
num1 = int(input("Ingrese un número entero: "))
num2 = int(input("Ingrese otro número entero: "))
operador = input("Qué operación desea realizar? Opciones: +, -, *, / --> ")

if operador == "+" or operador == "-" or operador == "/" or operador == "*":
    if operador == "+":
        print("Resultado: ", num1, operador, num2, "=", num1 + num2)
    elif operador == "-":
        print("Resultado: ", num1, operador, num2, "=", num1 - num2)
    elif operador == "*":
        print("Resultado: ", num1, operador, num2, "=", num1 * num2)
    elif operador == "/":
        print("Resultado: ", num1, operador, num2, "=", num1 / num2)
else:
    print("Por favor ingrese un operador válido")