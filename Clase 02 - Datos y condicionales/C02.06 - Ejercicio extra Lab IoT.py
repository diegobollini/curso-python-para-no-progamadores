# Python también se utiliza en IoT por ejemplo para programar entradas y salidas
# en Raspberry Pi. Utilizando el circuito con interruptores en serie y paralelo
# y una lámpara, indicar por mensajes en consola cuándo la lámpara está encendida
# o no. Los interruptores deben ser simulados con entradas por consola
# [Ejercicio](https://drive.google.com/file/d/1kgJl16gqqeveaOVVdvXa3UhpSoebHduV/view?usp=sharing)

enchufe = int(input("¿Enchufe (0/1)?: "))

if enchufe == 1:
    interruptor_a = int(input("¿Interruptor A (0,1)?: "))
    interruptor_b = int(input("¿Interruptor B (0,1)?: "))
    interruptor_c = int(input("¿Interruptor C (0,1)?: "))
    if interruptor_a == 1 or (interruptor_b == 1 and interruptor_c == 1):
        print("La lámpara está prendida")
    else:
        print("La lámpara está apagada")

else:
    print("La lámpara está desenchufada")