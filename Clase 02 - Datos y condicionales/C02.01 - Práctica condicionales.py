# True      True      and --> True      or --> True
# True      False     and --> False     or --> True
# False     False     and --> False     or --> False
# False     True      and --> False     or --> True

"""
>>> a=10
>>> b=100
>>> a>b
>>> a==b
>>> c=1000
>>> a > b and c > b
>>> c > b or a > c
>>> True and False
False
>>> False and True
False
>>> False and False
False
>>> not False
True
>>> not True
False
"""

num = 200
if num == 200:
    print("El número es igual a 200")
print("Esto se imprime sin importar la condición")

num = 201
if num == 200:
    print("El número es igual a 200")
print("Esto se imprime sin importar la condición")

# Caso edad para votar
edad = 20

if edad >= 65: #considerar el orden!
    print("Votación opcional")
elif  edad >= 16:
    print("Puede votar")
else:
    print("No puede votar")

# Condicionales anidados (atención a la cantidad de líneas)
edad2 = 4

if  edad2 >= 16:
    if edad2 >= 65:
        print("Votación opcional")
    else:
        print("Puede votar")
else:
    if edad2 < 6:
        print("Sos muy peque")
    else:
        print("No puede votar")