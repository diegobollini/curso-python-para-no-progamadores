# Ingresar por consola número de mes e imprimir nombre correspondiente.
# Si es un número no válido mostrar un mensaje de error.

mes = int(input("Ingresa un mes en número: "))

if mes == 1:
    print("Mes = Enero")
elif mes == 2:
    print("Mes = Febrero")
elif mes == 3:
    print("Mes = Marzo")
elif mes == 4:
    print("Mes = Abril")
elif mes == 5:
    print("Mes = Mayo")
elif mes == 6:
    print("Mes = Junio")
elif mes == 7:
    print("Mes = Julio")
elif mes == 8:
    print("Mes = Agosto")
elif mes == 9:
    print("Mes = Septiembre")
elif mes == 10:
    print("Mes = Octubre")
elif mes == 11:
    print("Mes = Noviembre")
elif mes == 12:
    print("Mes = Diciembre")
else:
    print("Ingrese un número válido")