# Insertar un número por consola e indicar si el mismo es o no par

num = int(input("Ingresa un número entero por favor: "))

if num % 2 == 0:
    print("El número es par")
else:
    print("El número es impar")