# Crear un programa que solicite el nombre de un alumno a través de la consola
# y luego chequee que no esté vacío.
# En caso de estarlo, debe imprimir un mensaje de error;
# caso contrario, imprimir un mensaje indicando que se ingresó correctamente.

nombre = input("Ingresa tu nombre: ")

if  nombre == "":
    print("Error: No se ingresó ningún nombre")
else:
    print("Correcto, gracias")