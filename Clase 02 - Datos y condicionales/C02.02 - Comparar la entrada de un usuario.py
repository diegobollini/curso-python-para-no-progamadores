# Crear un programa que permita ingresar dos cadenas vía la consola
# y luego las compare, imprimiendo un mensaje en caso que sean iguales
# y otro en caso que sean diferentes.
#
# Comentar bloques de código para ejecutar parcialmente

# Entrada de datos
input("Ingrese un número entero: ")

nombre = input("Escribe tu nombre: ")
print(type(nombre)) # es siempre una cadena

# ese input debe almacenarse
num = input("Ingrese un número entero: ")
if num == "100":
    print("El número es igual a 100")

# Conversión entre tipos de datos (de int a str)
num1 = int(input("Ingrese un número entero: "))
if num1 == 100:
    print("El número es igual a 100")

# Caso con dos entradas
entrada1 = input("Carga una palabra en minúsculas: ")
entrada2 = input("Carga otra palabra en minúsculas: ")

if entrada1 == entrada2:
    print("Efectivamente son iguales")
if entrada1 != entrada2:
    print("Las palabras son distintas")

# Caso con número entero
num = int(input("Elegí un número del 0 al 9 y vemos si adivinaste: "))
if num == 5:
    print("Ey, adivinaste!")
else:
    print("Mmm, nop")

# Entero y múltiples condicionales
num2 = int(input("Elegí un número y vemos si adivinas: "))

if num2 == 14:
    print("WTF, adivinaste!")
elif num2 < 14:
    print("Tu número es menor")
else:
    print("Tu número es mayor")
