#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# V3- Crear una variable y asignarle el valor de su nombre. Crear otra variable numérica entera y
# asignarle su edad. Mostrar en consola “Soy <<nombre>> y tengo <<edad>> años.

nombre="Diego"
edad=37
print("Soy", nombre, "y tengo ", edad, "años")
