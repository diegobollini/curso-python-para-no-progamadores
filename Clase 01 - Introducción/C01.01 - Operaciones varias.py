#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Operaciones varias

a=10
b=2
print(a,b)

c=a+b
print(c)

d=a-b
print(d)

e=a*b
print(e)

f=a/b
print(f)

g=b%a
print(g) #módulo: resto de la operación

h=a**b
print(h)

i=9**(1/2)
print(i)
