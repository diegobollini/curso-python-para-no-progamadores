#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# V1- Calcular el importe total a partir de variables precio_unitario y cantidad. Guardar el resultado
# en una nueva variable adicionándole además el IVA (21%).
# Mostrar el resultado por consola

precio_unitario=100
cantidad=3
resultado_neto=precio_unitario*cantidad
iva=resultado_neto*0.21
resultado_total=resultado_neto+iva
print("Precio: ", resultado_neto)
print("IVA: ", iva)
print("Total: ", resultado_total)
