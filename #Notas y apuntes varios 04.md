# Clase 04: Introducción a funciones (Estructura Alumni)
- diccionarios, operaciones varias
- funciones, implementación
- argumentos, valores de retorno
- formatos

### Colecciones
- lista [] > ordenable y modificable
- diccionario {} > modificable
- tupla () > ordenable
- set {} 

```python
tupla = (4,5,6)
tupla[0]
len tupla
# mismo comportamiento que lista pero no se puede modificar
```

## Diccionarios
- al no ser ordenados no se puede acceder por índice
- pares clave (no se repiten, str en general) : valor (cualquier tipo)

Por ejemplo:
```python
diccionario = {clave1 : valor1, clave2 : valor2, claven: valorn}

alumnos = {"nombre":"Juan", "cursos":3}
print(alumnos)
>>> {'nombre': 'Juan', 'cursos': 3}

print(alumnos["nombre"])
print(alumnos["cursos"])
>>> Juan
>>> 3

alumnos["cursos"]=5
print(alumnos)
>>> {'nombre': 'Juan', 'cursos': 5}

alumnos["curso_actual"]="Python"
print(alumnos)
>>> {'nombre': 'Juan', 'cursos': 3, 'curso_actual': 'Python'}

# Usando un bucle “for” sobre un diccionario tenemos acceso a cada una de sus claves
for clave in alumnos:
    print(clave) # imprime las claves
    print(alumnos[clave]) # imprime los valores
>>> nombre
>>> Juan
>>> cursos
>>> 5
>>> curso_actual
>>> Python
```

Otras operaciones sobre diccionarios:
```python
alumnos = {123: "Diego", 345: "Gatite", 553: "Danna"}
print(alumnos)

del alumnos[123] # borro un elemento a partir de una clave
print(alumnos)

alumnos[311] = "Nombre" # agrego un elemento (insert y append no aplican)
print(alumnos)
print(len(alumnos))
```

## Funciones
- ```input()```, ```print()```, ```range()``` son funciones ya incorporadas en Python.
- devuelven un valor
- permiten agrupar una o más líneas de código bajo un mismo nombre
- ```def``` + ```nombre_función``` + ```(atributos, parámetros):``` + instrucciones indentadas

```python
def saludo(): # defino la función
    print("Hola python")
    print("Hola Codium")
    # print("Hola, cómo estás?") # puedo agregar o quitar líneas

saludo() # llamar / invocar función
```

## Argumentos
- variables que existen sólo en una función cuyo valor indico cuando llamo a esa función

Por ejemplo, dos bucles ```for``` similares: recorren listas e imprimen:
```python
alumnos = ["Pablo", "Juan", "Matias", "Martin"]
notas = [5.5, 9, 6.25, 8]

print("Alumnos:")
for alumno in alumnos:
    print(alumno)

print("Notas:")
for nota in notas:
    print(nota)
```
Expresado de forma genérica:
```python
for elemento in lista: # lista == alumnos y notas
 print(elemento)
```
Utilizando funciones:
```python
# definimos una función, que requiere un argumento (en este caso "lista")
def imprimir_elementos(lista): # lista es la variable (parámetro en una función) a "llenar"
    for elemento in lista:
        print(elemento)

print("Lista de alumnos: ")
imprimir_elementos(alumnos) # llamamos a la lista alumnos
print("Lista de notas: ")
imprimir_elementos(notas) # llamamos a la lista notas
imprimir_elementos(["cualquier lista", 12, 34]) # podemos llamar a cualquier lista
```
Ahora bien, una función puede tener n argumentos (separados por comas):
```python
def una_funcion(a, b): # función que requiere dos argumentos
    print("Resultado de la función: ")
    print(a + b)

una_funcion(10, 5)
una_funcion("Hola", "mundo")
```

## Valor de retorno
- el resultado de una función se establece con ```return```
- determina el fin de la función (última línea de la función)
- puede ser cualquiera de los tipos de datos, incluso listas
Por ejemplo:
```python
def sumar(a, b): # función que requiere dos argumentos y el resultado es la suma de ambos
    return a + b
resultado = sumar(7, 5) # lo tengo que asignar a una variable

print(resultado) # >>> 12
print(sumar(-5, 3.5)) # >>> -1.5
```

### Diferencias return / print
- inicialmente, no usar ```print``` dentro de funciones
- return > cuál de las variables de la función tendrá el resultado
- print > para uso en consola principalmente
```python
def aplicar_iva(precio):
    precio_con_iva = precio * 1.21
    return precio_con_iva
    # return precio * 1.21
    # print(precio_con_iva) # limita los usos de la función
```

### Múltiples valores de retorno
- return > admite listas
```python
def min_max(lista):
    a = min(lista) # min y max funciones incorporadas en python
    b = max(lista)
    # return [a, b] # es una opción, suele ser más prolijo crear una variable
    resultado = [a, b]
    return resultado

print(min_max([1, 2, 5, 10]))
```

## Ámbito de las variables
Todas las variables que definimos en Python tienen un ámbito (lugar en el que es reconocida como tal, es visible para otras porciones de código)
- buena práctica: crear variables únicamente en el ámbito en el que son requeridas.
- local: todas las variables definidas dentro de una función
- global: variables definidas fuera de esa función (también se pueden llamar desde adentro)

Por ejemplo:
```python
a = 1 # global
def funcion():
    b = 2 # local, todo acceso por fuera de la variable dará error
    print(a)
    print(b)

funcion()
```
Defino una misma variable en los dos ámbitos:
```python
a = 1
def funcion():
    a = 2
    print(a) # imprime 2, predomina local

funcion()
```
Si bien no es una buena práctica y se debe evitar, puedo modificar variables globales desde una función:
```python
a = 1
def funcion():
    global a # defino el ámbito de la función, siempre el mismo
    a = 2

print(a) # 1
funcion()
print(a) # 2
```

## Formatos
- sep: separador de parámetros
- end: comportamiento al final de línea
Algunos ejemplos:
```python
print("Mi nombre es", nombre, "y tengo", edad,"años.", sep="")
print("Mi nombre es", nombre, "y tengo", edad,"años.", sep="-")
print("Mi","nombre","es",nombre,"y","tengo",edad,"años.", sep="-")
print("Mi nombre es", nombre, "y tengo", edad,"años.", sep=" ", end=".")
print("Línea debajo de las otras")
```