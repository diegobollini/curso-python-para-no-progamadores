# Clase 02: Tipos de datos (Estructura Alumni)
- datos y tipos
- operaciones aritméticas y lógicas
- condicionales
- comentarios

## Comentarios
- texto que no se ejecuta para explicar qué hace determinada línea o bloque de código
- se usa # para las líneas, """ o # ~ para bloques
```python
# Imprime mensaje en pantalla
print("Hola mundo")

# Instrucciones para mostrar en pantalla
""" print("Hola")
print("mundo") """
```

## Tipado dinámico
- en función del tipo de dato, se podrán aplicar ciertos atributos o ejecutar ciertas operaciones
```python
>>> a=10
>>> type(a)
<class 'int'> #integer
>>> a="Diego"
>>> type(a)
<class 'str'> #string
>>> b=4,5
>>> type(b)
<class 'tuple'> #collection
>>> c=4.5
>>> type(c)
<class 'float'> #float (decimal)
>>> d="Frase 1234 +"
>>> type(d)
<class 'str'>
>>> e=false
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'false' is not defined
>>> e=False
>>> type(e)
<class 'bool'> #booleano (True, False case sensitive)
```

## Operaciones aritméticas
```python
>>> 7+5
12
>>> a=5
>>> pi=3.14
>>> a+6
11
>>> a+pi
8.14
>>> a/pi
1.592356687898089
>>> 2+3*3
11
>>> (2+3)*3
15
>>> 2**3
8
>>> 4**2
16
```

## Operaciones lógicas y comparación
- mayor (>); menor (<); mayor o igual (>=); igual (==); distinto (!=)
- = y != aplican sobre cualquier tipo de dato
- el resultado siempre es booleano (True, False)

```python
>>> a = 10
>>> b = 100
>>> a > b
False
>>> a == b
False
>>> edad = 37
>>> edad >= 16
True
>>> puede_votar = edad >= 16
>>> puede_votar
True
```

### and (conjunción, ambas True)
```python
>>> True and True
True
>>> True and False
False
>>> False and True
False
```

### or (disyunción, al menos una True)
```python
>>> a = 5
>>> a == 1 or a == 10
False
>>> True or True
True
>>> True or False
True
```

### not
```python
>>> not True
False
>>> not False
True
>>> pi != 3.14
False
>>> not pi == 3.14
False
```

## Condicionales
### if - elif - else
```python
if condicion:
  se ejecuta esta línea
  y esta
  esta también
elif condicion: (las distintas condiciones se evalúan hasta que es True)
  línea
else...
```
```python
a = 12
if a > 10:
  print("Es mayor que 10.") #«indentación»
else:
  print("Es menor que 10")
```

## Entrada de datos
```python
nombre = input("Escribe tu nombre:")
print("Hola " + nombre)
numero = int(input("Ingresa un número: "))
```