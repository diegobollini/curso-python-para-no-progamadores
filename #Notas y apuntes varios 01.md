# Clase 01: Introducción al lenguaje (Estructura Alumni)
- introducción, fundamentos
- instalación, ambiente
- consola interactiva, terminal
- expresión, variable

## Introducción y definiciones
- python --> lenguaje
- python --> programa / intérprete --> código máquina

- [What Is Python? A Comprehensive Guide](https://www.netguru.com/what-is-python)
> It's versatile, which means it can be used in a variety of projects and across multiple
> industries, including data science, machine learning, blockchain, etc
- is an interpreted, general-purpose programming language
- it’s modular
- it’s open-source

## Preparar ambiente de trabajo
### Instalar python
```bash
$ sudo apt-get install python python 3.8
$ python3 -V
Python 3.8.5
```

### Instalar Codium en Mint / Ubuntu (visualcode fork sin M$💩)
```bash
$ wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium-archive-keyring.gpg
$ nano /etc/apt/sources.list.d/vscodium.list
deb [signed-by=/etc/apt/trusted.gpg.d/vscodium-archive-keyring.gpg] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main
$ apt install codium
```

### Opción: Geany
- Geany is a powerful, stable and lightweight programmer's text editor
- instalación, documentación [link](https://www.geany.org/)
- para cambiar la versión de python por defecto [link](https://stackoverflow.com/a/34317310)

## Python desde consola
```bash
$ python3.8
Python 3.8.5 (default, Jul 28 2020, 12:59:40) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
```
```python
>>> 7+5
12
>>> a=10
>>> a
10
>>> a=12
>>> b=2
>>> a
12
>>> b
2
>>> a+b
14
>>> a="Hola mundo"
>>> a
'Hola mundo'
```

## Conceptos fundamentales
- sintaxis > orden y relación de los términos
- expresión > fragmento de código que ejecuta una operación y retorna un resultado; el lenguaje "reemplazará" ese fragmento de código por su resultado (es decir, siendo 7 + 5 la expresión, python lo "reemplaza" por 12)

```python
7 + 5 #expresión
print(7 + 5) #"reemplaza" la operación por el resultado (12)
```
- variable > nombre de una expresión (las variables son expresiones)
- definición > crear una variable
```python
variable = expresión #por convención, nombre de variable en minúscula
suma = 7 + 5
```