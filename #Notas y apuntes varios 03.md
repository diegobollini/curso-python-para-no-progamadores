# Clase 03: Introducción a bucles (Estructura Alumni)
- listas y matrices
- bucles
- conversiones tipos de datos

## Listas
- colección ordenada de objetos, separados con ","
- permiten agrupar "elementos" de cualquier tipo de dato
- elementos ordenados, cada uno tiene un índice (posición, desde 0)
```python
>>> numeros_pares = [2, 4, 6, 8, 10]
>>> socies = ["Nombre1", "Nombre2","Nombre3","Nombre4"]
>>> datos = [3.14, True, "Nombre", 2020, "Un texto"]
>>> nombre_lista.append(elemento)
>>> nombre_lista.insert(indice, elemento)
>>> socies
>>> socies[2]
>>> del nombre_lista[indice]
>>>  len(nombre_lista)
```

## Matrices
- lista con listas como elementos
```python
>>> mi_lista = [[3.14, "Hola mundo"], [True, False, -5]]
>>> mi_lista[0]
[3.14, 'Hola mundo']
>>> mi_lista[1]
[True, False, -5]
>>> mi_lista[0][1]
'Hola mundo'
>>> mi_lista[1][0]
True
>>> mi_lista[1][1]
False
>>> m = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9]
]
>>> m [2][1]
8
>>> m [2][1] = 18
>>> m [2][1]
18
```

## Bucles
- repiten un bloque de código cuando se cumpla una condición.
 
### while
- repite una porción de código siempre que una expresión == True (requiere una condición)
```python
a = 1
while a < 5:
 print("Hola mundo") # como siempre es True, se imprime infinitamente
```
Por eso es necesario que la variable de la condición cambie:
```python
a = 1
while a < 5:
    print("Hola mundo") # se va a imprimir 4 veces, hasta que a < 5 es False
    a = a + 1 # también se puede probar con a = 6 para que corte el bucle
```
También se puede finalizar un bucle con ```break```, sin importar la condición.
```python
a = 1
while True: # ejecución infinita
    if a < 5:
        print("Hola mundo")
        a = a + 1 #cuando a = 5 --> break
    else:
        break
```

### for
- repite la ejecución tantas veces como elementos contenga una lista
- útil cuando quiero ejecutar una operación para cada elemento de una lista (por ejemplo enviar un mail a personas inscriptas de una lista)
- SIEMPRE se ejecuta sobre listas
Por ejemplo:
```python
>>> alumnos = ["Nombre01", "Nombre02", "Nombre03", "Nombre04"]
>>> for alumno in alumnos:
...     print("Hola mundo")
... 
Hola mundo
Hola mundo
Hola mundo
Hola mundo
```
```python
meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]

for mes in meses: # puede ser cualquier nombre de variable (mes)
  print(mes) # imprime todos los elementos

for what in meses:
  print(what) # también imprime la lista
```
De hecho, sería lo mismo ejecutar:
```python
meses = ["Enero", "Febrero", "Marzo",...]
mes = meses[0]
print (mes)
mes = meses[1]
print (mes) # for se encarga de realizar automágicamente las asignaciones a la variable
...
```

### range
- genera listas de números enteros en tiempo de ejecución
Por ejemplo, quiero imprimir los números del 1 al 10:
```python
i = 1
while i <= 10:
    print(i)
    i = i + 1
```
Con for debería armar la lista... ¿pero si son 100 números? --> range
```python
numeros = range(1,101)
for numero in numeros: # cambio el 11 por 101,1001, etc.
    print(numero)
```
Puedo simplificar si la variable ```numero``` no se utilizará:
```python
for numero in range(1,101):
  print(numero)
```

Otros ejemplos de uso de ```range```:
```python
range(desde, hasta, incremento) #genera lista numérica de enteros según parámetros desde, hasta -1
range(0, 5, 1) # 0, 1, 2, 3, 4
range(0, 5, 2) # 0, 2, 4
range(1, 10, 1) # 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
range(5, -1, -1) # 5, 4, 3, 2, 1, 0
range(5) # por defecto empieza de 0 e incrementa 1
```
```python
>>> for contador in range(0,5,1):
...    print("Hola" + str(contador))
Hola0
Hola1
Hola2
Hola3
Hola4
```

### continue & break
- se utilizan siempre dentro de un bucle, en general ```for```
```python
primos = [1, 3, 5, 7, 11, 13]
for numero in primos:
  if numero == 5:
    continue #omite la ejecución para esta condición y sigue con el próximo elemento
  print(numero)
print("Fin del programa")
```
```python
primos = [1, 3, 5, 7, 11, 13]
for numero in primos:
    if numero == 5:
      break #abortar el bucle
  print(numero)
print("Fin del programa")
```

## Conversiones
- operación muy común
Por ejemplo convertir flot a int:
```python
pi = 3.14
pi_int = int(pi)
print(pi, " - ", pi_int)
```
Se utiliza sobre todo conversión int <--> str
- usamos ```int``` para operaciones aritméticas y ```str``` si queremos concatenar con otras variables y mostrar un mensaje 
```python
num_str = "123"
num_int = int(num_str)
print(type(num_str), " - ", type(num_int))
print(num_str, " - ", num_int)
# print("El precio es " + num_int) # esta instrucción dará error
print("El precio es " + num_str)
```
Por ejemplo:
```python
>>> celular = "153656666"
>>> celular_prefijo = "+54" + celular
>>> celular_prefijo
'+54153656666'
>>> celular_siguiente = celular + 1
TypeError: can only concatenate str (not "int") to str
>>> celular_siguiente = int(celular) + 1
>>> celular_siguiente
153656667
>>> celular_siguiente_prefijo = "+54" + str(celular_siguiente)
>>> celular_siguiente_prefijo
'+54153656667'
```

# Integrando conceptos
## Matrices
```python
lista = [1, 2, 3.4, True, "Palabras"]
# cualaquier tipo de dato, entonces puedo tener listas de listas!

listados = [[4,5,6], [7,8,9]] # matriz de 2 x 3 (2 listas de 3 elementos)
# lista regular, misma cantidad de elementos

print(listados[0])
# [4, 5, 6]
print(listados[0][0])
# 4
print(listados[1][1])
# 8
```

### Matrices como tablas
- Cada lista es una fila
- Cada elemento una columna
```python
for fila in range (0, 2, 1): # empiezo a recorrer la matriz, fila posición 0
    for columna in range (0, 3, 1): # empiezo a recorrer la matriz, columna posición 0 hasta completar
        print(listados[fila][columna])

# Versión simplificada
for fila in listados:
    for columna in fila:
        print(columna)

# Exactamente igual al código anterior
for listita in listados:
    for elemento in listita:
        print(elemento)
```

### Práctica, loops, menúes con while
```python
num = 1
while num < 10:
    print(num)
    num = num + 1
```
```python
num = 1
while num < 10:
    print(num)
    num = num + 1
    if num == 5:
        break
```
```python
while True: # bucle infinito
    print("1 - Hacer algo")
    print("2 - Hacer otra cosa")
    print("3 - Salir")
    opcion = int(input("Ingrese una opción: "))
    if opcion == 1:
        print("Una cosa")
    elif opcion == 2:
        print("Una cosa 2")
    elif opcion == 3:
        break
    else:
        print("Error")
```