#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# App desktop básica con ventana, botón y etiqueta, que imprime un mensaje en consola.

import tkinter as tk # importar librería, primer línea del código, tk == abreviatura

# Funciones para interacciones
def boton_presionado():
	nombre = caja_nombre.get() # para tomar el valor en la caja de texto, se guarda en variable
	# get retorna siempre un string
	print(nombre)

def boton_hola():
    print("Hola mundo")

# función Tk(), crea ventana y asigna a variable "ventana"
ventana = tk.Tk()

ventana.config(width = 300, height = 300) # tamaño en píxeles
ventana.title("Primera app desktop con python") # título de la ventana, "tk" por defecto

# Para crear un control debemos escoger el nombre de una variable,
# indicar qué tipo de control queremos crear,
# darle un tamaño y ubicarlo en algún lugar de la interfaz.
# Para generar interacción, llamamos a una función con command
boton_hola = tk.Button(text="Hola mundo", command = boton_hola)
boton_hola.place(x=20, y=40, width=100, height=30)
# todos los botones deben especificar los 4 valores

# crear control = caja de texto (entry)
caja_nombre = tk.Entry() # no requiere argumentos, incluyen get() que retorna str
caja_nombre.place(x = 20, y = 120, width = 200, height = 25) # siempre 4 argumentos para place
etiqueta = tk.Label(text = "Ingresa tu nombre") # etiquetas sólo muestran textos
etiqueta.place(x = 20, y = 90)

boton_imprimir = tk.Button(text = "Imprimir", command = boton_presionado)
boton_imprimir.place(x = 20, y = 160) # el tamaño se calcula automáticamente

# hace visible a la ventana, "escucha" los eventos en esa ventana, última línea del código
ventana.mainloop() 