# Requerimiento: https://docs.google.com/document/d/1Hyaew3UkVyhWaFvZsALr6y6G_SLbBiEhHYHE8B2E3Cc/edit
# Ejercicio integrador base: https://gitlab.com/diegobollini/curso-python-para-no-progamadores/-/blob/master/Clase%2004/C04.03%20-%20Ejercicio%20Integrador%20C04.05%20iv7.py

import tkinter as tk

alumnos = {}

def lista_alumnos():
    for nombre in alumnos:
        cursos = alumnos[nombre]
        print(nombre + " - " + str(cursos) + " cursos")

def ingresar_alumno():
    nombres = caja1.get()
    cursos = caja2.get()
    alumnos[nombres] = cursos

def buscar_cursos():
    nombres = caja1.get()
    if nombres in alumnos:
        print("Alumna/o " + nombres + " registrada/o en " + str(alumnos[nombres]) + " cursos")
    else:
        print("El nombre no figura en la base.")

ventana = tk.Tk()
ventana.config(width = 400, height = 300)
ventana.title("Proyecto Integrador - App Alumnos 2020")

boton1 = tk.Button(text = "Ver lista de alumnas/os", command = lista_alumnos)
boton1.place(x = 10, y = 10)

etiqueta1 = tk.Label(text = "Nombre alumna/o:")
etiqueta1.place(x = 20, y = 60)
caja1 = tk.Entry(justify = tk.CENTER)
caja1.place(x = 150, y = 60, width = 150, height = 25)

etiqueta2 = tk.Label(text = "Cursos:")
etiqueta2.place(x = 20, y = 90)
caja2 = tk.Entry(justify = tk.CENTER)
caja2.place(x = 150, y = 90, width = 150, height = 25)

boton2 = tk.Button(text = "Agregar a la lista", command = ingresar_alumno)
boton2.place(x = 10, y = 150)

boton3 = tk.Button(text = "Ver cantidad de cursos", command = buscar_cursos)
boton3.place(x = 180, y = 150)

ventana.mainloop()