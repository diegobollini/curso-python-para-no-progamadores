# Requerimiento: https://docs.google.com/document/d/1Hyaew3UkVyhWaFvZsALr6y6G_SLbBiEhHYHE8B2E3Cc/edit
# Ejercicio integrador base: https://gitlab.com/diegobollini/curso-python-para-no-progamadores/-/blob/master/Clase%2004%20-%20Funciones%20y%20colecciones/C04.03%20-%20Ejercicio%20Integrador%20C04.05%20iv7.py
# Solución de clase: https://drive.google.com/file/d/1glKwvPLAFjZANGNH5RMvC-KxNYhO_yIp/view

import tkinter as tk

alumnos = {}

ventana = tk.Tk()
ventana.config(width = 500, height = 350, bg = "#dce5f2")
ventana.title("Proyecto App Alumnos 2020 ver 2.0")

def mostrar_lista():
    print("Lista de alumnas/os:")
    for nombre in alumnos:
        print(nombre + " - " + alumnos[nombre] + " cursos")

def agregar_registros():
    nombres = caja_nombre.get()
    if nombres == "":
        print("Por favor ingrese un nombre.")
    else:
        cursos = caja_cursos.get()
        alumnos[nombres] = cursos
        print("¡Listo, alumna/o añadido a la lista!")

def buscar():
    nombres = caja_nombre.get()
    if nombres in alumnos:
        print("Alumna/o " + nombres + " registrada/o en " + str(alumnos[nombres]) + " cursos")
    else:
        print("El nombre no figura en la base.")

def salir():
    ventana.destroy()

boton_lista = tk.Button(text="Ver lista de inscriptas/os", command = mostrar_lista)
boton_lista.place(x=20, y=30)

caja_nombre = tk.Entry()
caja_nombre.place(x = 150, y = 100, width = 200, height = 25)
etiqueta_nombre = tk.Label(text = "Nombre alumna/o:", bg = "#dce5f2")
etiqueta_nombre.place(x = 20, y = 100, height = 25)

caja_cursos = tk.Entry()
caja_cursos.place(x = 150, y = 140, width = 100, height = 25)
etiqueta_cursos = tk.Label(text = "Cant. de cursos:", bg = "#dce5f2")
etiqueta_cursos.place(x = 20, y = 140, height = 25)

boton_agregar = tk.Button(text="Agregar a la lista", command = agregar_registros)
boton_agregar.place(x=20, y=200)

boton_buscar = tk.Button(text="Buscar cantidad de cursos", command = buscar)
boton_buscar.place(x=200, y=200)

boton_salir = tk.Button(text="Salir", bg = "#f5ae58", command = salir)
boton_salir.place(x=200, y=280)

ventana.mainloop()