# ~ Crear una aplicación de escritorio con dos cajas de texto y un botón,
# ~ de modo que al presionarlo se imprima en pantalla la suma de los dos números ingresados.

import tkinter as tk

def sumar():
    num1 = float(caja_num1.get())
    num2 = float(caja_num1.get())
    print(num1 + num2)

# Variación función + resultado en app
def calcular():
	num3 = float(caja_num1.get())
	num4 = float(caja_num2.get())
	resultado.config(text = str(num3 + num4))
	print(num3 + num4)

ventana = tk.Tk()
ventana.title("Súper calculadora científica")
ventana.config(width = 300, height = 250)

caja_num1 = tk.Entry()
caja_num1.place(x=20, y=50, width=100, height=25)
etiqueta_num1 = tk.Label(text="Número 01")
etiqueta_num1.place(x=20, y=20, width=100)

caja_num2 = tk.Entry()
caja_num2.place(x=150, y=50, width=100, height=25)
etiqueta_num2 = tk.Label(text="Número 02")
etiqueta_num2.place(x=150, y=20, width=100)

boton_sumar = tk.Button(text="Sumar", command = calcular) # variación def sumar()
boton_sumar.place(x=100, y=100)

# Variación función + resultado en app
etiqueta_resultado = tk.Label(text = " = ")
etiqueta_resultado.place(x = 120, y = 160)
resultado = tk.Label(text = "0.00")
resultado.place(x = 116, y = 200)

ventana.mainloop()