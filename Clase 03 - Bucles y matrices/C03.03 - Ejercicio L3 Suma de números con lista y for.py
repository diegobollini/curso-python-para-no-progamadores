# L3- A partir de una lista de números calcular la suma de sus elementos

numeros = [2, 3, 10, 5.5]

""" for numero in numeros:
  sum(numero) """
# TypeError: 'int' object is not iterable

""" for numero in numeros:
    suma = suma + numero
print(suma) """
# NameError: name 'suma' is not defined

suma = 0

for numero in numeros: #como convención, nombre de variable es el singular de la lista
    suma = suma + numero
    print("subtotal:", suma)
print("Fin del bucle.")
print("total:", suma)
