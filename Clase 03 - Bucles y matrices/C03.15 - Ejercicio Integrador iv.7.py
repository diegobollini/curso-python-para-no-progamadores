# Ejercicio Integrador iv.7
# Link: https://docs.google.com/document/d/1jKGAnurmLX0LmQDAUqIRr4GCjmU8wVkW7m2slOv0oXU/edit
# Solución: https://drive.google.com/file/d/1NK6PwOUsQWixFk-DSu1OvUR8xwXdXfu2/view

# ~ Lista de dos elementos (el primero el nombre, el segundo la cantidad de cursos como un número entero)
# ~ en una lista de alumnos.

# ~ Ingrese el número de la operación que desea ejecutar:
# ~ 1 - Ver la lista de alumnos.
# ~ 2 - Añadir un alumno a la lista.
# ~ 3 - Salir.

alumnos = []

while True:
    print("Bienvenidx al sistema de gestión de alumnas/os:")
    print("1 - Ver la lista de alumnas/os.")
    print("2 - Añadir alumna/o a la lista.")
    print("3 - Salir.")
    opcion = int(input("Ingrese una opción: "))
    
    if opcion == 1:
        print("Lista de alumnas/os:")
        for alumno in alumnos:
            nombres = alumno[0]
            cursos = alumno[1]
            print(nombres + " - " + str(cursos) + " cursos")
    elif opcion == 2:
        nombres = input("Ingrese el nombre de alumna/o: ")
        if nombres == "":
            print("Por favor ingrese un nombre.")
        else:
            cursos = int(input("Ingrese la cantidad de cursos: "))
            alumnos.append([nombres, cursos])
            print("¡Listo, alumna/o añadido a la lista!")
    elif opcion == 3:
        print("¡Gracias por utilizar este súper programa, vuelva prontos!")
        break
    else:
        print("--> La opción ingresada no es correcta, vuelva a intentarlo <--")
