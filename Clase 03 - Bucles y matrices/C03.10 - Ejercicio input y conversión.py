# Preguntar edad, validar si es int e indicar si puede o no votar

edad = ""

# Bucle hasta llegar a un valor válido
while type(edad) == str:
    edad = input("Ingresa tu edad por favor: ")
    if edad.isdecimal(): # si el valor es número, lo convierto a int para comparar
        edad = int(edad)
    else:
        print("Valor ingresado incorrecto.")

if edad > 16:
    print("Puedes votar en elecciones nacionales en Argentina.")
else:
    print("Aun no puedes votar, gracias por tu interés!")