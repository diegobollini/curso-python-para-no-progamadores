# L1- Ingresar por consola número de mes e imprimir el nombre de mes correspondiente
# (Ejemplo: 2 -> Febrero).
# Si es un número no válido mostrar un mensaje de error.
# Resolverlo utilizando una lista.

#!/usr/local/bin/python
# -*- coding: utf-8 -*-

meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]

mes = int(input("Ingresa el número de mes: "))

if mes < 1 or mes > 12:
    print("Ingrese un número válido")
else:
    print("El mes elegido es", meses[mes - 1])