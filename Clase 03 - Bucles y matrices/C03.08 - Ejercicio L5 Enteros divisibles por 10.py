# L5- Encontrar en una lista de números enteros los divisibles por 10.
# Por ejemplo numeros = [100,5,8,56,99,10]

numeros = [100, 5, 8, 56, 99, 10, 4, 1000, 300, 160]

# Acá una versión guardando los valores en una variable, aunque no es necesario.
# Importante: no mostrar listas a usuarios, sino recorrerla e imprimir cada elemento
# Revise el "else" que estaba mal indentado

""" divisibles = []

for numero in numeros:
    if numero % 10 == 0:
        divisibles.append(numero)
else:
    print("Fin del bucle") #Para chequear el bucle

print("Los números de la lista divisibles por 10 son:", divisibles) """

## Versión revisada
for numero in numeros:
    if numero % 10 == 0:
        print(numero, "es divisible por 10")
    else:
        print(numero, "no es divisible por 10") # a los efectos del ej. no hace falta
