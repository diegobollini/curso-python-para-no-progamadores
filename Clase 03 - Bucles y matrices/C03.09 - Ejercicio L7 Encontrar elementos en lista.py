# L7- Encontrar si un elemento se encuentra en la lista
# e imprimir la primer posición en que aparece

lista = [1, 2, 3, 1, 5, 6, 2, 3, 5, 6]

incognita = int(input("Elegí un elemento: "))

# Busqué en Google y uso "in" e "index"
""" if incognita in lista:
    print("El elemento está en la lista y la primer posición es:", lista.index(incognita))
else:
    print("El elemento no está en la lista") """

# Acá lo trato de hacer con for
for numero in lista:
    if numero == incognita:
        print("Incógnita en lista, posición: ", lista.index(incognita))
        break
print("Fin del bucle")
