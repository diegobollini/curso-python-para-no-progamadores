# Practicando con Listas

alumno1 = "Diego"
alumno2 = "Danna"
alumno3 = "Gatite"
alumnos = ["Diego", "Danna", "Gatite"]
print(alumnos) # en lugar de print alumno1, print alumno2, etc

numeros_pares = [2, 4, 6, 8, 10]
socies = ["Nombre1", "Nombre2","Nombre3","Nombre4"]
datos = [3.14, True, "Nombre", 2020, "Un texto"]

# Para acceder a elementos
print(socies[2])
print(datos[3])

# Para agregar elementos
## nombre_lista.append(elemento)
numeros_pares.append(12)
socies.append("Nombre5")

print(socies)
# >>> ['Nombre1', 'Nombre2', 'Nombre3', 'Nombre4', 'Nombre5']

## nombre_lista.insert(indice, elemento)
## insert es menos usual que append
socies.insert(1, "Apellido 1")
print(socies)
# >>> ['Nombre1', 'Apellido 1', 'Nombre2', 'Nombre3', 'Nombre4', 'Nombre5']

## reemplazar un elemento
socies[3] = "Nombre corregido"

## del nombre_lista[indice]
del socies[1]
print(socies)
socies.remove("Nombre3") #elimina la primera vezq que aparece este valor (repetir si hay más)

eliminado = socies.pop(2)
print(eliminado)
print(socies)

##  len(nombre_lista)
print(len("Hola mundo"))
print(len(socies))
# >>> 5

## Accede al último elemento.
print(numeros_pares[len(numeros_pares) - 1])
# >>> 12