# Crear un programa que solicite una fila y una columna
# e imprima en pantalla el número en esa posición según la matriz que se adjunta.
# Ejemplo de entrada (lo que escribe el usuario) y salida (lo que se imprime en pantalla):
# Fila: 1
# Columna: 2
# 6.4
# El resultado es 6.4 puesto que es el valor ubicado en matriz[1][2].
# El programa debe chequear que la fila y la columna tengan valores válidos.
# En este caso, las únicas filas válidas son 0 y 1; las columnas, 0, 1 y 2.
# Si alguno de los dos valores es inválido, debe mostrar un mensaje de error.

matriz = [[3.3, 6.1, 4.0], [4.9, 5.7, 6.4]]

fila = int(input("Ingrese un número de fila: "))

if fila == 0 or fila == 1:
    columna = int(input("Ingrese un número de columna: "))
    if columna < 0 or columna > 2:
        print("Error en número de columna.")
    else:
        print(matriz[fila][columna])
else:
    print("Error en número de fila.")
