# Crear un programa que permita insertar una matriz,
# y que ésta sea almacenada como una lista de Python.
# Esta matriz contiene dos filas y tres columnas, datos requeridos por el programa.
# Luego nuestro código procederá a solicitar los números de cada celda.

# La entrada y salida del programa para este ejemplo sería similar a la siguiente:
# Ingrese el número de filas: 2
# Ingrese el número de columnas: 3
# Ingrese el número para matriz[0][0]: 5
# Ingrese el número para matriz[0][1]: 1
# Ingrese el número para matriz[0][2]: 15
# Ingrese el número para matriz[1][0]: 10
# Ingrese el número para matriz[1][1]: 9
# Ingrese el número para matriz[1][2]: 6
# La matriz ingresada es:
# [[5, 1, 15], [10, 9, 6]]

# Este fue mi primer intento, me faltó mostrar cada posición al hacer los inputs
""" matriz = []

filas = int(input("Ingrese cantidad de filas: "))
columnas = int(input("Ingrese cantidad de columnas: "))

for fila in range (0, (filas) , 1):
    matriz.append([])
    for columna in range (0, (columnas), 1):
        matriz[fila].append(int(input("Ingrese un número para la matriz: ")))

print("La matriz ingresada es:")
print(matriz) """


# Solución en clase
matriz = []
filas = int(input("Ingrese el número de filas: "))
columnas = int(input("Ingrese el número de columnas: "))

for numero_fila in range(0, filas):
    matriz.append([])
    for numero_columna in range(0, columnas):
        numero = int(input("Ingrese el número para matriz[" + str(numero_fila) + "][" + str(numero_columna) + "]: "))
        matriz[numero_fila].append(numero)

print("La matriz ingresada es:")
print(matriz)