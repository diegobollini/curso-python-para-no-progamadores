# L6-¿ Cuál es el número menor de una lista de números cualquiera?

# Acá si ingreso yo la lista
lista = []

for numeros in range(0,5,1):
    numero = int(input("Ingrese un número entero:"))
    lista.append(numero)

print(min(lista))

# Solución en clase
numeros = [100, 5, 89, 4, 70]
menor = numeros[0]

for pos in range(1, len(numeros), 1): #no empiezo el bucle desde el índice 0 porque no se compara con sí mismo
    if numeros[pos] < menor:
        menor = numeros[pos]
print("El menor número es: ", menor)
