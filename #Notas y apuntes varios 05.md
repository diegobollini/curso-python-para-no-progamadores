# Clase 05: Introducción a aplicaciones de escritorio (Estructura Alumni)
- qué es una aplicación de escritorio
- herramientas
- ventanas, controles
- interacciones usuario / código

## Introducción
- aplicación de escritorio: programa que establece una interacción con el usuario a través de una interfaz gráfica
- componentes --> ventana, controles (widgets): botones, cajas de texto, menúes, ventanas...

### Herramientas
- a diferencia de las funciones "nativas" como print() o input(), se deben instalar librerías (archivos que contienen funciones).
- python --> módulos --> carpeta con módulos --> paquetes
- por ejemplo, [30 Best Python Libraries and Packages](https://www.ubuntupit.com/best-python-libraries-and-packages-for-beginners/) o [A curated list of awesome Python frameworks, libraries, software and resources](https://github.com/vinta/awesome-python)
- otras librerías para apps desktop: wxWidgets, Qt, GTK
  
Por ejemplo:
```python
import math
import time
import sys

print(math.sqrt(9)) # float
# >>> 3.0
time.sleep(2) # ejecución pausada por 2 segundos
print(math.factorial(5))
# >>> 120

print(sys.version) # python version
# >>> 3.8.5 (default, Jul 28 2020, 12:59:40)
print(sys.platform)
# >>> linux
```

### Librería tkinter (ventana)
- para los ejercicios de práctica se utiliza Tcl/Tk (al menos en mi Mint 20.04 la instale con ```$ apt-get install python-tk```)
- The tkinter package (“Tk interface”) is the standard Python interface to the Tk GUI toolkit.
- running ```python3 -m tkinter``` from the command line should open a window demonstrating a simple Tk interface
- entonces, para utilizar librerías se agrega al inicio del código ```import [librería]```

```python
import tkinter as tk
ventana = tk.Tk()
ventana.title("Primera aplicación de escritorio")
ventana.config(width = 300, height = 300)
ventana.mainloop() # bloquea ejecución
```

## Controles
Nombre de una variable (como con ventana), indicar qué tipo de control queremos crear, darle un tamaño y ubicarlo en algún lugar de la interfaz.
- primero se crea, luego se ubica y define tamaño
  
Creando un botón:
```python
boton_hola = tk.Button(text="Hola mundo")
boton_hola.place(x=20, y=40, width=100, height=30)
```
Creando caja de texto y etiqueta:
```python
caja_nombre = tk.Entry() # "input", no requiere argumentos
caja_nombre.place(x=20, y=120, width=200, height=25)
etiqueta_nombre = tk.Label(text="Ingresa tu nombre:", bg = "#5AEA1B") # bg == background color
etiqueta_nombre.place(x=20, y=90)
```

### Interacción con interfaz
- Paradigma: programación orientada a eventos
  
Utilizando un botón:
```python
def boton_hola():
    print("Hola mundo")
...
boton_hola = tk.Button(text="Hola mundo", command = boton_hola)
```
Utilizando una caja de texto obteniendo su contenido:
```python
def imprime_nombre():
	nombre = caja_nombre.get() # siempre retorna un str
	print(nombre)

boton_nombre = tk.Button(text="Imprimir nombre", bg = "#3625D9", command = imprime_nombre)
boton_nombre.place(x=20, y=160)
```

# Cierre - Certificación
Algunas preguntas / respuestas de la certificación final:
- las ```expresiones``` son porciones de código que siempre retornan un resultado, a las que opcionalmente les damos un nombre (variable)
- todo ```condicional``` debe tener al menos un bloque de código indentado para distinguirlo del código que está fuera de él. Además, la primera palabra de un condicional siempre debe ser if
- puede crearse una variable con el nombre print, aunque luego no podrá utilizarse como la instrucción "nativa"
- las ```sentencias``` se distinguen de las ```expresiones``` por no retornan ningún resultado. Python no reemplazará ningún valor cuando aparezca a = 12, ya que es una definición y las definiciones son sentencias. Pero sí lo hará cuando encuentre 7 + 5, ya que es una expresión, que reemplazará por 12
- el bucle ```for``` está fundado internamente sobre el bucle ```while``` y por lo tanto puede reducirse a este.