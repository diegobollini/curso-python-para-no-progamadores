# Crear una función rango(desde, hasta, intervalo) que retorne una lista de números,
# tal como la función incorporada range(), aunque según el intervalo especificado.
# Por ejemplo, el siguiente código:

""" lista = rango(1, 10, 2)
print(lista)

debe imprimir:
[1, 3, 5, 7, 9]
puesto que se genera una lista desde 1 hasta 10 con un intervalo de 2."""

# Por ejemplo, la siguiente función simula el comportamiento de range():

""" def rango(desde, hasta):
    numeros = []
    while desde < hasta:
        numeros.append(desde)
        desde = desde + 1
    return numeros

lista = rango(1, 6)
print(lista) """

# valor de retorno de rango(1, 6) es la lista [1, 2, 3, 4, 5].

def rango(desde, hasta, intervalo):
    numeros = []
    while desde < hasta:
        numeros.append(desde)
        desde = desde + intervalo
    return numeros

lista = rango(1, 50, 5)
print(lista)