# ¿Y si quiero usar múltiples argumentos?

def imprimir_saludo_mult(destinatario, lenguaje, editor):
    print("Hola " + destinatario)
    print("desde " + lenguaje)
    print("y " + editor)

# Ejecutar función respetando el orden de los argumentos en la definición
imprimir_saludo_mult("Diego", "Python", "Geany")
imprimir_saludo_mult("Gatite", "Ruby", "Codium")

# Salvo que defina explícitamente cada valor (no se estila usar)
imprimir_saludo_mult(editor = "Geany", destinatario = "Diego", lenguaje = "Java")