# L7- Encontrar si un elemento se encuentra en la lista
# e imprimir la primer posición en que aparece.

lista = [1, 2, 3, 1, 3, 2, 5, 4, 5, 6]

incognita = int(input("Elija un elemento a buscar en la lista: "))

def busqueda(lista):
    contador = 0
    for numero in lista:
        contador = contador + 1
        if numero == incognita:
            print("Elemento en la posición: ", contador - 1)
            break

busqueda(lista)

# Busqué en Google y se puede usar index
print("Incógnita en lista, posición: ", lista.index(incognita))

# ¿Qué pasa si el elemento no está en la lista? [PENDIENTE]
# Convendría retornar la posición. ¿Qué pasa si ahora necesitas saber todas las ocurrencias?
# Si no está en la lista podrías retornar -1 o None