## Verificar si existe el metal en el diccionario

metales = {"oro":1000, "plata":100, "bronce":10}

while True:
    metal = input("Selecione el tipo de metal: ")
    if metal in metales:
        kilos = float(input("Ingrese la cantidad de kilos: "))
        print("El importe total es =", (kilos * metales[metal]))
        pregunta = input("Desea seguir calculando? S / N: ")
        if pregunta == "N":
            break
    else:
        print("Error, metal no encontrado, ingrese nuevamente.")
