#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

nombre = "Diego"
edad = 37

# Impresión concatenada
print("Mi nombre es " + nombre + " y tengo " + str(edad) + " años.")

# Bajando líneas
print(nombre)
print(edad)

# Admite varias variables
print(nombre, edad) # no da error porque no estoy concatenando
print("Mi nombre es", nombre, "y tengo", edad,"años.")

# ~ No uso espacios porque se incluye la variable sep, que si está vacía == espacio (sep=" ")
# ~ Por ejemplo:
print("Mi nombre es", nombre, "y tengo", edad,"años.", sep="")
print("Mi nombre es", nombre, "y tengo", edad,"años.", sep="-")
print("Mi","nombre","es",nombre,"y","tengo",edad,"años.", sep="-")

print("Mi nombre es", nombre, "y tengo", edad,"años.", sep=" ", end=".")
# por defecto end = "/n", es decir pasar a la otra línea
print("Línea debajo de las otras") # entonces al asignarle un valor, no baja la línea
