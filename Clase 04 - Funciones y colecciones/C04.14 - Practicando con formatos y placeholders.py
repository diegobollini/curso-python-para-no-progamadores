#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Placeholders {}

nombre = "Diego"
edad = 37

print("Soy {0} y tengo {1} años".format(nombre,edad))
print("Soy {nombre} y tengo {edad} años")
print(f"Soy {nombre} y tengo {edad} años")

## int con ancho fijo (mínimo de 5)
print("{0:5d}".format(12))
#>>>   12
# 3 espacios + int = 5 (:5d), el 0 es porque aplica sobre el id 0 de format
# (por defecto si no de define)

print("{0:5d}{1:5d}".format(12,123))
#>>>   12  123

# Sirve por ejemplo para alinear int de distinta cantidad de cifras
print("{0:5d}".format(123))
print("{0:5d}".format(12))
print("{0:5d}".format(12123))


## int con ancho fijo (mínimo de 5 y 0 para completar, para facturas por ejemplo)
print("{0:05d}".format(123))
print("{0:05d}".format(12))
print("{0:05d}".format(12123))

## ancho mínimo de 8 con 3 decimales
print("{0:8.3f}".format(123.456))
print("{0:8.3f}".format(12.3456)) # redondea sólo al mostrar
print("{0:8.3f}".format(123))
print("{0:8.3f}".format(123456.789))

# Aplicando placeholders
print("el número {0} se muestra sin decimales así {1:1.0f}".format(12.3456,12.3456))

print("Esta aplicación es {0:d}% {1}".format(100, "segura"))
porcentaje = 80
print("Esta aplicación es {0:d}% {1}".format(porcentaje, "segura"))

# Secuencia de escape
print("{0:5.3f} \t {1:5.3f}".format(123,456)) #\t == tabulador
print("{0:5.3f} \t {1:5.3f}".format(567,890))