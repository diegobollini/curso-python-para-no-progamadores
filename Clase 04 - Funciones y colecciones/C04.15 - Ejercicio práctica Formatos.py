#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# ~ Solicitar nombre de productos y precio (float) por consola y guardar en un diccionario.
# ~ Precios entre 1 y 100000, sino inválido y hasta que esté OK.
# ~ Luego de cada alta consultar si desea ingresar nuevo producto.
# ~ Si es no, listar productos y precios (alineados a la derecha con dos decimales).
# ~ Por ejemplo:
	# ~ Mouse - $  500.00
	# ~ Teclado - $ 1500.50
	# ~ Monitor - $10000.00


base_datos = {}

while True:
	producto = input("Ingrese el producto por favor: ")
	while True:
		precio = float(input("Ingrese su precio: "))
		if precio >= 1 and precio <= 100000:
			base_datos[producto] = precio
			print("Producto dado de alta!")
			break
		else:
			print("Precio no válido: ")
	nuevo = input("Desea ingresar un nuevo producto? S / N = ")
	if nuevo == "N":
		break

for items in base_datos:
	print("{0}\t {1:9.2f}".format(items, base_datos[items]))
