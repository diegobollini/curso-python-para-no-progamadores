# Función que simula "range()"

def rango(desde, hasta):
    numeros = []
    while desde < hasta:
        numeros.append(desde)
        desde = desde + 1
    return numeros


lista = rango(1, 6)
print(lista)
# resultado o valor de retorno de rango(1, 6) es la lista [1, 2, 3, 4, 5]

# Función que requiere una lista como argumento y retorna su último elemento
def ultimo_elemento(lista):
    return lista[len(lista) - 1]

mi_lista = [1, 2, 3, 4, 5]
print(ultimo_elemento(mi_lista)) # >>> 5