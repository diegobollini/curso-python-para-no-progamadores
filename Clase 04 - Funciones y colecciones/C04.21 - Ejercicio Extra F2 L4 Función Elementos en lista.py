# L4- Calcular la cantidad de elemento que tiene una lista (sin utilizar len).

def contar(lista):
    contador = 0
    for elementos in lista:
        contador = contador + 1
    return contador

print("Cantidad de elementos de la lista: ")
print(contar([1,2,3,4,5]))

print("Cantidad de elementos de la lista: ",(contar(["uno","dos","tres"])))