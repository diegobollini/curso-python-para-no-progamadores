# Crear una función que tome como argumento una lista
# (considerando que siempre contendrá números enteros y de coma flotante)
# y retorne la suma de todos sus elementos.
# Por ejemplo, el siguiente código:

""" print(sumar([1, 2, 3, 4, 5]))

debe imprimir:
15
pues resulta de hacer 1 + 2 + 3 + 4 + 5. """

# primero defino la función
def sumar(lista):
    sumando = 0 # variable para sumas parciales al recorrer la lista
    for elemento in lista:
        sumando = sumando + elemento # recorre y suma
    return sumando # valor final del "recorrido"

resultado = sumar([10, 10]) # acá si asigno a  una variable el resultado
print(resultado)

lista = [1, 2, 3, 4, 5]
print(sumar(lista)) # sumando a partir de una lista ya declarada

print(sumar([2, 4, 6])) # declarando una lista específica
