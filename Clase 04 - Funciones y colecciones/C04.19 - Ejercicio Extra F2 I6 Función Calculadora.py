""" I6- Calculadora: Ingresar dos números y un operador(+,-,*,/) por consola
e imprimir el resultado de la siguiente manera:
Resultado: 3 + 4 = 7 """

# Review: No le estás pasado parámetros a la función.
# Ver commit anterior para modificaciones

def calculadora(num1, operador, num2):
    if operador == "+":
        resultado = num1 + num2
    elif operador == "-":
        resultado = num1 - num2
    elif operador == "*":
        resultado = num1 * num2
    elif operador == "/":
        resultado = num1 / num2
    else:
        print("Ingrese un operador válido.")
    return resultado

num1 = float(input("Ingrese el primer número: "))
num2 = float(input("Ingrese el segundo número: "))
operador = input("Ingrese el operador matemático que desea usar (+,-,*,/): ")

#calculadora()
print("Resultado: ", str(num1), operador, str(num2), "=", calculadora(num1, operador, num2))