# Modelo básico de función

def suma(numero1, numero2):
    resultado = numero1 + numero2
    print("El resultado es: " + str(resultado))
    # cuando se utiliza print, se suele llamar procedimiento

# Ejecución base
a = 10
b = 5
suma(a, b)
""" >>> El resultado es: 15 """

c = 4
d = 6
suma(c, d)
""" >>> El resultado es: 10 """

# Ejecución alternativa
def suma2(numero1, numero2):
    resultado2 = numero1 + numero2
    return resultado2

# Agrego texto en el resultado
a = 2
b = 3
resultado2 = suma2(a, b)
print("El resultado de esta suma es: " + str(resultado2))

# Sumo un tercer número
c = 2
d = 5
e = 7
resultado2 = suma2(c, d)
resultado3 = suma2(resultado2, e)
print("El resultado con 3 números es: " + str(resultado3))