# Definir función (no ejecuta)

def imprimir_saludo():
    print("Hola python")
    print("Hola Codium")
    print("Hola, cómo estás?")

# Ejecutar función
imprimir_saludo()

a = 5
b = 10
print (a + b)

# Nueva ejecución
imprimir_saludo # sin () no tira error pero no ejecuta nada
imprimir_saludo()

# ¿Y si quiero cambiar los argumentos?

def imprimir_saludo_var(destinatario):
    print("Hola " + destinatario)
    print("¿Cómo estás?")

# Ejecutar función
imprimir_saludo_var("Diego")
imprimir_saludo_var(destinatario = "Gatite")
""" print(destinatario) # NameError: name 'destinatario' is not defined """
