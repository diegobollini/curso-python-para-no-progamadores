# D2 -Escribe un programa que pida un número por consola y que cree un diccionario
# cuyas claves sean desde el número 1 hasta el número indicado incluido,
# y los valores sean los cuadrados de las claves. 

diccionario = {}
numero = int(input("Ingrese un número entero: "))

for valor in range(1, numero + 1, 1):
    diccionario[valor] = valor ** 2
    print(diccionario)

print(diccionario)