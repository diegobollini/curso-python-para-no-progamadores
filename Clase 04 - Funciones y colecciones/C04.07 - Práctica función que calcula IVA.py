# Función para calcular el precio con IVA
def aplicar_iva(precio):
    precio_iva = precio * 1.21 # precio_iva definida sólo dentro de la función
    return precio_iva

# print(precio_iva) # error!
aplicar_iva(100)
print(aplicar_iva(100))

resultado = aplicar_iva(100) # entonces le asigno a una variable el resultado de la función

print(resultado)

# Evolución: si precio < 100, exento de IVA
def aplicar_iva2(precio2):
    if precio2 < 100:
        precio_iva2 = precio2
    else:
        precio_iva2 = precio2 * 1.21 # precio_iva definida sólo dentro de la función
    return precio_iva2

aplicar_iva2(123)
print(aplicar_iva2(99))

resultado2 = aplicar_iva2(100) # entonces le asigno a una variable el resultado de la función
print(resultado2)