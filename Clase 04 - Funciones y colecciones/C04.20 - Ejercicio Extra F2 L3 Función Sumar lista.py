# L3- A partir de una lista de números calcular la suma de sus elementos.

def sumando(lista):             # defino la función, con una lista como parámetro
    total = 0                   # variable para acumular subtotales
    for numeros in lista:       # recorro cada elemento de la lista
        total = total + numeros # y voy acumulando la suma
    return total                # retorno total (suma de los números de la lista)

lista = [1,2,3,4,5]
print(sumando(lista))           # llamo a la función con una lista ya definida

print(sumando([10,10,0.5]))     # defino una lista al llamar a la función