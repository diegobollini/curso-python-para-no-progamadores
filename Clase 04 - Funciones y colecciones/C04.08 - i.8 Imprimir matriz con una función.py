# Crear una función que tome como argumento una matriz de cualquier dimensión,
# e imprima cada uno de sus números en una línea diferente.
# Por ejemplo, el siguiente código:

""" m1 = [[3.3, 6.1, 4.0], [4.9, 5.7, 6.4]]
imprimir_matriz(m1)

debe imprimir:

3.3
6.1
4.0
4.9
5.7
6.4 """

def imprimir_matriz(un_nombre_matriz):
    # si usara range debería contar también la longitud, etc. etc.
    for fila in un_nombre_matriz:
        for columna in fila:
            print(columna)

m1 = [[3.3, 6.1, 4.0], [4.9, 5.7, 6.4]]

imprimir_matriz(m1)