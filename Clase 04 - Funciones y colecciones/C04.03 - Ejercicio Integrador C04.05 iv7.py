#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ~ La lista de alumnos que habíamos creado en la clase anterior ahora debe ser un diccionario,
# ~ en donde las claves serán nombres de alumnos y los valores sus respectivas cantidad de cursos.
# ~ Para esto deberemos modificar el código de las opciones 1 y 2
# ~ (ver la lista de alumnos y agregar un nuevo alumno).
# ~ Luego agregaremos una cuarta opción que será “Ver la cantidad de cursos de un alumno”.
# ~ Deberá solicitar el nombre de un alumno e imprimir en pantalla el número de cursos
# ~ que tiene asociados como clave.

# Crear un diccionario vacío.
alumnos = {}

# Ejecutar el siguiente código infintamente --> Menú
while True:
    print("Bienvenidx al sistema de gestión de alumnas/os:")
    print("1 - Ver la lista de alumnas/os.")
    print("2 - Añadir alumna/o a la lista.")
    print("3 - Ver cantidad de cursos de alumna/o.")
    print("4 - Salir.")
    opcion = int((input("Ingrese una opción: ")))
    
    if opcion == 1:
        print("Lista de alumnas/os:")
        # El bucle "for" aplicado sobre un diccionario recorre sus claves
        for nombre in alumnos:
            # Par clave (nombre) - valor (cursos)
            cursos = alumnos[nombre]
            # Convertir a str cursos (int) para concatenar (al ejecutar tira error sino)
            print(nombre + " - " + str(cursos) + " cursos")

    elif opcion == 2:
        nombres = input("Ingrese el nombre de alumna/o: ")
        # Verificar que al menos se ingresa un valor para nombre
        if nombres == "":
            print("Por favor ingrese un nombre.")
        else:
            cursos = int(input("Ingrese la cantidad de cursos: "))
            # Agregar un nuevo par clave - valor al diccionario alumnos.
            alumnos[nombres] = cursos
            print("¡Listo, alumna/o añadido a la lista!")

    elif opcion == 3:
        buscar = input("Ingrese el nombre a buscar: ")
        if buscar in alumnos:
            print("Alumna/o registrada/o en " + str(alumnos[buscar]) + " cursos")
        else:
            print("El nombre no figura en la base.")

    elif opcion == 4:
        print("¡Gracias por utilizar este súper programa, vuelva prontos!")
        break
        
    else:
        print("--> La opción ingresada no es correcta, vuelva a intentarlo <--")
