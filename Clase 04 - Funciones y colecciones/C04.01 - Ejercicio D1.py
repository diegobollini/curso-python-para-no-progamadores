# D1- Crea un programa donde se debe declarar un diccionario con los precios de distintos metales.
# El programa pedirá el nombre del metal y los kilos que se ha vendido y nos mostrará
# el precio final del metal. Tras cada consulta el programa nos preguntará si queremos
# hacer otra consulta. 

metales = {"oro":1000, "plata":100, "bronce":10}

while True:
    metal = input("Selecione el tipo de metal: ")
    kilos = float(input("Ingrese la cantidad de kilos: "))
    print("El importe total es =", (kilos * metales[metal]))
    pregunta = input("Desea seguir calculando? S / N: ")
    if pregunta == "N":
        break