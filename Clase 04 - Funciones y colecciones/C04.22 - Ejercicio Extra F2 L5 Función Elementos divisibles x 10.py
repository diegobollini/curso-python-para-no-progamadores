# L5- Encontrar en una lista de números enteros los divisibles por 10.
# Ej. numeros=[100,5,8,56,99,10]

numeros = [100,5,8,56,99,10]


def divisibles(lista):
    son_divisibles = []
    for elementos in lista:
        if elementos % 10 == 0:
            son_divisibles.append(elementos)
    return son_divisibles


print("Los números divisibles por 10 son: ", divisibles(numeros))

# Busqué cómo imprimir la lista con mejor formato
print("Los números divisibles por 10 son: ", *divisibles([9,143,153250,144,330]))