# F1 - Generar una función que dado un número retorne su factorial

# ~ El factorial de n se define como el producto de todos los números enteros positivos desde 1
# ~ (es decir, los números naturales) hasta n

## Solución en internet
def factorial(n): 
    resultado = 1
    i = 1
    while i <= n:
        resultado = resultado * i
        i = i + 1
    return resultado

print(factorial(6))

## Solución clase
def factorial2(num):
    total = 1
    for i in range(1, num + 1, 1):
        total = total * i
    return total

print(factorial2(6))