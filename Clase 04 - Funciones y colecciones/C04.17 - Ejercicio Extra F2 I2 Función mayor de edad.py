# I2 - Solicitar una edad por consola indicar si la persona es mayor de edad (18 años o más)
# Si además tiene 65 años o más, mostrar por consola que puede contar con la jubilación

"""
# Condiciones
edad < 18: no puede votar
edad > 18: puede votar
edad > 65: jubilación
 """

edad = int(input("Por favor ingrese su edad: "))

if edad < 0:
    print("Ingrese una edad válida.")

else:
    def edades(edad):
        if edad < 18:
            print("Persona menor de edad.")
        elif edad > 18 and edad < 65:
            print("Persona mayor de edad.")
        else:
            print("Mayor de edad y corresponde jubilación.")

edades(edad)

