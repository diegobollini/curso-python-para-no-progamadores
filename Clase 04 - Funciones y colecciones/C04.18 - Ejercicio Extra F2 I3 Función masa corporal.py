""" I3- Una medida de la obesidad se determina mediante el índice de masa corporal (IMC), que se
calcula dividiendo los kilogramos de peso por el cuadrado de la estatura en metros (IMC = peso
[kg]/ estatura [m2])
Escribir un programa que pida al usuario su peso (en kg) y estatura (en metros) y de acuerdo a la
siguiente tabla calcule el índice de masa corporal, lo almacene en una variable, y muestre por
pantalla el índice de masa corporal calculado redondeado con dos decimales y la composición
corporal. """
# Composición corporal Índice de masa corporal (IMC)
# Peso inferior al normal Menos de 18.5
# Normal 18.5 – 24.9
# Peso superior al normal 25.0 – 29.9
# Obesidad Más de 30.0

# Review: La función debería calcular sólo el imc y el resto de los condicionales estar en el programa.
# Ver commit anterior para modificaciones

def calculo(peso, estatura):
    return round(peso / (estatura ** 2), 2)

peso = float(input("Ingrese su peso (kg.): "))
estatura = float(input("Ingrese su altura (metros): "))

if calculo(peso, estatura) > 0 and calculo(peso, estatura) < 18.5:
    print("Su peso es inferior al normal y su índice de masa corporal es ", calculo(peso, estatura),)
elif calculo(peso, estatura) >= 18.5 and calculo(peso, estatura) <= 24.9:
    print("Su composición corporal es normal, su índice de masa corporal es ", calculo(peso, estatura),)
elif calculo(peso, estatura) > 24.9 and calculo(peso, estatura) < 30:
    print("Su composición corporal es superior a lo normal, su índice de masa corporal es ", calculo(peso, estatura),)
elif calculo(peso, estatura) >= 30:
    print("Su composición es Obesidad y su índice de masa corporal es ", calculo(peso, estatura),)
else:
    print("Por favor ejecute nuevamente el programa")
